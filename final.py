import pprint

import openpyxl
import re


def is_email(email):
    regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return True if re.fullmatch(regex, str(email)) else False


def is_integer(row_id):
    return isinstance(row_id, int)


def is_float(sales):
    return isinstance(sales, float)


class ReadFile:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data

    def clean_data(self, data):

        wrong_data = []
        right_data = []
        cont = 1

        for items in data:
            cont += 1
            if is_email(items['email']) and is_integer(items['row_id']) and is_float(items['sales']):
                right_data.append(items)
            else:
                wrong_data.append(items)
                items['Indice registro erroneo'] = cont

        return wrong_data, right_data

    def validar_ship_mode(self, data):
        dic = {}
        for items in data:
            if dic.get(items.get('ship_mode')):
                dic.get(items.get('ship_mode')).append(items)
            else:
                dic[items.get('ship_mode')] = [items]
        return dic


def main():
    test = ReadFile('/home/lsv/Escritorio/workspace/Sample.xlsx')
    pprint.pprint(test.clean_data(test.read_file()))
    pprint.pprint(test.validar_ship_mode(test.read_file()))

    dirty, clear = test.clean_data(test.read_file())
    #print('Cantidad de registros correctos: ', len(clear))
    #print('Cantidad de registros erroneos:', len(dirty))




if __name__ == '__main__':
    main()
